<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Pendaftaran</title>
</head>

<body>
    <!-- judul -->
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <!-- buat form Pendaftaran -->
    <form action="/register" method="POST">
        @csrf
        <!-- input nama -->
        <p>First Name: </p>
        <input type="text" placeholder="Masukkan Nama Depan" name="firstname"><br>
        <p>Last Name: </p>
        <input type="text" placeholder="Masukkan Nama Belakang" name="lastname"><br>
        <!-- tanggal lahir -->
        <p>Tanggal Lahir</p>
        <input type="date" name="ttl">
        <!-- input jenis kelamin -->
        <p>Gender: </p>
        <input type="radio" name="jenis-kelamin">Male<br>
        <input type="radio" name="jenis-kelamin">Female<br>
        <input type="radio" name="jenis-kelamin">Other<br>
        <!-- input negara -->
        <p>Nationality: </p>
        <select name="negara">
            <option value="id">Indonesian</option>
            <option value="spn">Spanish</option>
            <option value="ger">Germany</option>
            <option value="eng">England</option>
            <option value="aus">Australia</option>
        </select><br>
        <!-- input bahasa -->
        <p>Language Spoken: </p>
        <input type="checkbox" name="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="bahasa">English<br>
        <input type="checkbox" name="bahasa">Other<br>
        <!-- input texarea -->
        <p>Bio: </p>
        <textarea name="bio" cols="90" rows="10" placeholder="Description Your Personality"></textarea><br>
        <!-- tombol submit -->
        <input type="submit" value="Register">

    </form>
</body>

</html>
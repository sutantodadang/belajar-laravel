<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
        return view('form');
    }

    public function register(Request $request)
    {
        $namadepan = $request->firstname;
        $namabelakang = $request->lastname;

        // dd($namadepan, $namabelakang);

        return view('/welcome', compact('namadepan', 'namabelakang'));
    }
}
